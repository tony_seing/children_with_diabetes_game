getParameterByName = (name)->
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]")
  regexS = "[\\?&]" + name + "=([^&#]*)"
  regex = new RegExp(regexS)
  results = regex.exec window.location.search
  if results == null
      ""
  else
      decodeURIComponent results[1].replace(/\+/g, " ")

$(document).ready ->
        if getParameterByName("choice") == "b"
                $('.wrong').attr('src', 'images/question_1_wrong2.png')
                $('.yourchoice').attr('src', 'images/question_1_btn2_on.png')
        else
                $('.yourchoice').attr('src', 'images/question_1_btn1_on.png')


