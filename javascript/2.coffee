getParameterByName = (name)->
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]")
  regexS = "[\\?&]" + name + "=([^&#]*)"
  regex = new RegExp(regexS)
  results = regex.exec window.location.search
  if results == null
      ""
  else
      decodeURIComponent results[1].replace(/\+/g, " ")

$(document).ready ->
        if getParameterByName("choice") == "3"
                $('.wrong').attr('src', 'images/question_2_wrong2.png')
                $('.yourchoice').attr('src', 'images/question_2_btn3_on.png')
        else
                $('.yourchoice').attr('src', 'images/question_2_btn1_on.png')


